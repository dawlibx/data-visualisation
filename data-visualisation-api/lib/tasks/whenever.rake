# frozen_string_literal: true

# whenever tasks used for background processing
namespace :whenever do
  namespace :bitcoin_usd_rate do
    desc 'Download realtime Bitcoin to USD exchange rate'
    task download: :environment do
      ::BitcoinUsdRateDownloadWorker.perform_async
    end
  end
end
