# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


############################################################################

# API for providing historical Bitcoin to USD currency exchange rate

$ bundle

$ bundle exec sidekiq

$ rails s

when using whenever:
$ whenever --update-crontab


# TODO

- controller for fetching BTC to USD exchange rate data
- presenter with fields mapping: exchange_rate -> rate etc. + not sending all the unnecessary fields
