# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BitcoinUsdRateDownloaderService do
  describe '#download!' do
    let(:bitcoin_usd_rate) do
      VCR.use_cassette('bitcoin_usd_rate_data') do
        subject.download!
      end.attributes
    end

    it 'has proper values under given attributes' do
      expect(bitcoin_usd_rate['from_currency_code']).to eq('BTC')
      expect(bitcoin_usd_rate['from_currency_name']).to eq('Bitcoin')
      expect(bitcoin_usd_rate['to_currency_code']).to eq('USD')
      expect(bitcoin_usd_rate['to_currency_name']).to eq('United States Dollar')
      expect(bitcoin_usd_rate['exchange_rate']).to eq('17175.04246190')
      expect(bitcoin_usd_rate['last_refreshed']).to eq('2017-12-12 23:36:09')
      expect(bitcoin_usd_rate['time_zone']).to eq('UTC')
    end
  end

  describe '#parse_response' do
    let(:response_body) do
      {
        'Realtime Currency Exchange Rate' =>
        {
          '1. From_Currency Code' => 'BTC',
          '2. From_Currency Name' => 'Bitcoin',
          '3. To_Currency Code' => 'USD',
          '4. To_Currency Name' => 'United States Dollar',
          '5. Exchange Rate' => '17175.04246190',
          '6. Last Refreshed' => '2017-12-12 23:36:09',
          '7. Time Zone' => 'UTC'
        }
      }.to_json
    end
    let(:parsed_response) do
      {
        'from_currency_code' => 'BTC',
        'from_currency_name' => 'Bitcoin',
        'to_currency_code' => 'USD',
        'to_currency_name' => 'United States Dollar',
        'exchange_rate' => '17175.04246190',
        'last_refreshed' => '2017-12-12 23:36:09',
        'time_zone' => 'UTC'
      }
    end

    it { expect(subject.send(:parse_response, response_body)).to eq(parsed_response) }
  end

  describe '#parse_key' do
    let(:key) { '1. From_Currency Code' }

    it { expect(subject.send(:parse_key, key)).to eq('from_currency_code') }
  end
end
