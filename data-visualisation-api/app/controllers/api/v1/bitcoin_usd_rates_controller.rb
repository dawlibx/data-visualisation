# frozen_string_literal: true

module Api
  module V1
    class BitcoinUsdRatesController < ApplicationController

      def index
        bitcoin_usd_rates = BitcoinUsdRatesService.new.fetch(params)

        Api::V1::BitcoinUsdRatesPresenter.create(bitcoin_usd_rates).to_json
      end
    end
  end
end
