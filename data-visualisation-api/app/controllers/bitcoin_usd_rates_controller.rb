# frozen_string_literal: true

class BitcoinUsdRatesController < ApplicationController
  def index
    bitcoin_usd_rates = BitcoinUsdRatesService.fetch(params)

    Api::V1::BitcoinUsdRatesPresenter.create(bitcoin_usd_rates).to_json
  end
end
