# frozen_string_literal: true

# Model representing Bitcoin to USD exchange rate
class BitcoinUsdRate
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic
end
