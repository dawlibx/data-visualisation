# frozen_string_literal: true

# Service responsible for downloading latests Bitcoin currency exchange rates
class BitcoinUsdRateDownloaderService
  # Raises when there is other response code than 200 from Realtime Currency Exchange Rate API
  class BitcoinUsdRateDownloadError < StandardError; end

  # Key of hash that represents Bitcoin exchange rate
  RATE_LABEL = 'Realtime Currency Exchange Rate'
  # URL for request to Realtime Currency Exchange Rate API
  # for BTC to USD rate exchange information.
  # Uses API host url and API key from application settings.
  URL = System::Settings.alpha_vantage.url +
        'query?function=CURRENCY_EXCHANGE_RATE&' \
        'from_currency=BTC&to_currency=USD&apikey=' +
        System::Settings.alpha_vantage.api_key

  # Requests for Bitcoin realtime currency exchange rate data and saves it into database.
  # @return [BitcoinUsdRate] Bitcoin to USD exchange rate data
  # @example
  #   BitcoinUsdRateDownloaderService.new.download!
  #   => #<BitcoinUsdRate _id: 5a2ee9fa09362636ec0a8193, created_at: 2017-12-11 20:26:34 UTC,
  #   updated_at: 2017-12-11 20:26:34 UTC, from_currency_code: "BTC",
  #   from_currency_name: "Bitcoin", to_currency_code: "USD",
  #   to_currency_name: "United States Dollar", exchange_rate: "17370.22242850",
  #   last_refreshed: "2017-12-11 20:26:34", time_zone: "UTC">
  def download!
    # Alternatively HTTParty gem could be used here, but VCR gem doesn't support it
    response = Faraday.get(URL)

    raise BitcoinUsdRateDownloadError unless response.status == 200

    BitcoinUsdRate.create!(parse_response(response.body))
  end

  private

  # @param response_body [String] body of response from Realtime Currency Exchange Rate API.
  # @return [Hash] parsed response in a desired format
  def parse_response(response_body)
    JSON.parse(response_body)[RATE_LABEL].each_with_object({}) do |(key, value), parsed_rate_data|
      parsed_rate_data[parse_key(key)] = value
    end
  end

  # Parses single key from exchange rate data from API in order to present it in a desired format.
  # @param key [String] key to parse
  # @return [String] parsed key
  # @example
  #   parse_key("1. From_Currency Code")
  #   => "from_currency_code"
  def parse_key(key)
    key.sub(/^\d+\.\s/, '').underscore.tr(' ', '_')
  end
end
