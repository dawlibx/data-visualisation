class BitcoinUsdRatesService

  DEFAULT_PERIOD = 7.days

  def fetch(params)
    period = params['period'] ? params['period'].days : DEFAULT_PERIOD

    # BitcoinUsdRate.where(:last_refreshed.gte => DateTime.now - period)
    BitcoinUsdRate.where(:created_at.gte => DateTime.now - period).to_a
  end
end
