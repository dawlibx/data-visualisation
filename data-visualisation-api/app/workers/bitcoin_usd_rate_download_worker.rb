# frozen_string_literal: true

# Worker responsible for downloading Bitcoin to USD exchange rate
class BitcoinUsdRateDownloadWorker
  include Sidekiq::Worker

  # Performs asynchronous job for downloading Bitcoin to USD exchange rate
  def perform(*_args)
    BitcoinUsdRateDownloaderService.new.download!
  end
end
